<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');
});

Auth::routes();

Route::get('/manager/dashboard', 'ManagerController@dashboard')->name('manager/dashboard');
Route::get('/manager/selectors', 'SelectorController@dashboard')->name('manager/selectors');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin/dashboard', 'HomeController@dashboard')->name('admin/dashboard');
Route::get('/admin/module/{slug}', 'ModuleController@index')->name('admin/module/view');
Route::get('/admin/module/{slug}/changestatus/{redirectTo?}', 'ModuleController@changeStatus')->name('admin/module/changestatus');
Route::post('/admin/module/setting/update', 'ModuleController@updateSetting')->name('admin/module/setting/update');

Route::get('/admin/products', 'ProductController@index')->name('admin/products');
//installScriptTags
Route::get('/admin/installscripts', 'HomeController@installScriptTags')->name('admin/install/scripts');

// Route::get('/admin/tasks/list', 'ModuleController@index')->name('admin/task/list');

Route::get('login/shopify', 'Auth\LoginShopifyController@redirectToProvider')->name('login.shopify');
Route::get('login/shopify/callback', 'Auth\LoginShopifyController@handleProviderCallback');

Route::get('stores/{storeId}', function() {
    // Show store dashboard
});


Route::get('js/{storeId}/{storeKey}', 'PublicController@js' )->name('js/code');
Route::get('{jsFile}.js', 'PublicController@theJs')->name('the/js');

Route::get('espresso/{storeId}/{storeKey}/app.js', 'PublicController@jsFile' )->name('jsfile');
Route::get('api/v1/update/record/{storeId}/{recordName}', 'PublicController@updateRecord')->name('api/v1/update/record');


Route::get('api/v1/visitor/info', 'PublicController@visitorData')->name('visitorData');
Route::get('api/v1/cart/get', 'PublicController@getCart')->name('v1/cart/get');


Route::get('stores/{storeId}/shopify/subscribe', function(\Illuminate\Http\Request $request, $storeId) {


    $store = \App\Store::find($storeId);
    // $user = auth()->user()->providers->where('provider', 'shopify')->first();
     $user = Auth::user();
     $provider = \App\UserProvider::where('user_id', $user->id)->where('provider', 'shopify')->first();

    $shopify = \Shopify::retrieve($store->domain, $provider->provider_token);

    $activated = \ShopifyBilling::driver('RecurringBilling')
        ->activate($store->domain, $provider->provider_token, $request->get('charge_id'));

    $response = array_get($activated->getActivated(), 'recurring_application_charge');

    \App\Charge::create([
        'store_id' => $store->id,
        'name' => 'default',
        'shopify_charge_id' => $request->get('charge_id'),
        'shopify_plan' => array_get($response, 'name'),
        'quantity' => 1,
        'charge_type' => \App\Charge::CHARGE_RECURRING,
        'test' => array_get($response, 'test'),
        'trial_ends_at' => array_get($response, 'trial_ends_on'),
    ]);

    return redirect('/home');

})->name('shopify.subscribe');

Route::post('webhook/shopify/uninstall', function(\Illuminate\Http\Request $request) {
    // Handle app uninstall
})->middleware('webhook');

Route::post('webhook/shopify/gdpr/customer-redact', function(\Illuminate\Http\Request $request) {
    // Remove customer data
})->middleware('webhook');

Route::post('webhook/shopify/gdpr/shop-redact', function(\Illuminate\Http\Request $request) {
    // Remove shop data
})->middleware('webhook');

Route::post('webhook/shopify/gdpr/customer-data', function(\Illuminate\Http\Request $request) {
    // Provide data on customer
})->middleware('webhook');
