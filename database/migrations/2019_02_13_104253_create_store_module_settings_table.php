<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreModuleSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //each module has some settings that are required to have the module working
        Schema::create('module_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('module_id');
            $table->string('label')->default('Label')->nullable();
            $table->string('s_key')->default(NULL)->nullable();
            $table->string('s_value')->default(NULL)->nullable();
            $table->string('s_value_type')->default('text')->nullable(); //input-integer //input-string //textarea //option (opt1,opt2,opt3)
            $table->string('s_options')->default(NULL)->nullable();
            $table->boolean('is_color_picker')->default(false)->nullable();
            $table->string('description');
            $table->timestamps();
        });

        //each store has modules enabled, and each module enabled has a blue print of his settings, per store
        Schema::create('store_module_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id');
            $table->integer('module_id');
            $table->string('s_key')->default(NULL)->nullable();
            $table->string('s_value')->default(NULL)->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_module_settings');
        Schema::dropIfExists('module_settings');

    }
}
