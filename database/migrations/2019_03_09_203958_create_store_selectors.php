<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreSelectors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_selectors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id');
            $table->integer('selector_id');
            $table->string('selector_key')->default(NULL)->nullable();
            $table->string('selector_value')->default(NULL)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_selectors');
    }
}
