<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id');
            $table->biginteger('product_id');
            $table->string('title')->nullable();
            $table->string('handle')->nullable();
            $table->text('body_html')->nullable();
            // $table->timestamp('published_at');
            $table->string('vendor')->nullable();
            $table->string('product_type')->nullable();
            $table->string('tags')->nullable();
            $table->text('images')->nullable();
            $table->timestamps();

            $table->unique('product_id');
            $table->index(['store_id', 'product_id', 'handle']);

        });
    }

    public function down()
    {
        Schema::dropIfExists('products');
    }
}
