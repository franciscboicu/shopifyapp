<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleStoreDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_store_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id');
            $table->integer('module_id');
            $table->string('data_key');
            $table->string('data_type');
            $table->string('data_string');
            $table->integer('data_integer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_store_data');
    }
}
