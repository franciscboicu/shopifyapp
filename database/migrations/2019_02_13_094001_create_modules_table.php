<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration
{

    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('name');
            $table->string('short_description');
            $table->string('full_description')->default(NULL)->nullable();
            $table->boolean('is_enabled')->default(FALSE)->nullable();
            $table->text('pictures_json')->default(NULL)->nullable();
            $table->string('namespace_class')->default(NULL)->nullable();
            $table->integer('order');
            $table->timestamps();
        });

          Schema::create('store_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id');
            $table->integer('module_id');
            $table->boolean('enabled')->default(false)->nullable();
            $table->integer('order');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('modules');
        Schema::dropIfExists('store_modules');

    }
}
