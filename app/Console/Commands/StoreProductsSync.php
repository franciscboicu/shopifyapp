<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Store;
use App\User;
use App\Product;

use PHPShopify\ShopifySDK;


class StoreProductsSync extends Command
{
    protected $signature = 'shopify:store:products:sync {storeUrl?}';

    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        if(NULL !== $this->argument('storeUrl')) {
            $this->info('[StoreProductsSync Start]');

            $store = Store::where('domain', $this->argument('storeUrl'))->first();

            if(!$store) {
                $this->error('[ERROR] Store ' . $this->argument('storeUrl') . ' not found');
                exit();
            }

            $this->syncStoreProducts($store);


            exit(0);
        }

        $stores = Store::where('domain', '!=', '')->get();

        foreach($stores as $store) {

            $this->syncStoreProducts($store);

        }

        $this->info($this->argument('storeUrl'));
    }


    private function syncStoreProducts($store) {

        $this->info('Starting Sync for ' . $store->domain);

        $user = User::find($store->users()->first()->id);

        $provider = $user->provider();

        $config = [
            'ShopUrl'       => $store->domain,
            'AccessToken'   => $provider->provider_token,
        ];

        $shopifyApi = ShopifySDK::config($config);

        $storeProducts = $shopifyApi->Product->get();

        $totalProductsFromApi = count($storeProducts);

        $iteratedProducts = 0;

        foreach($storeProducts as $storeProduct) {

            $iteratedProducts++;

            $minCondition = [
              'product_id' => $storeProduct['id'],
              'store_id'  => $store->id
            ];

            $product = Product::firstOrCreate($minCondition);

            $product->title = $storeProduct['title'];
            $product->handle = $storeProduct['handle'];
            $product->body_html = $storeProduct['body_html'];
            $product->vendor = $storeProduct['vendor'];
            $product->product_type = $storeProduct['product_type'];
            $product->tags = $storeProduct['tags'];
            $imgs = [];
            if ( is_array($storeProduct['images']) ) {
              foreach($storeProduct['images'] as $_img) {
                $imgs[$_img['position']] = [ 'src' => $_img['src'],
                                              'alt' => $_img['alt'],
                                              'id' => $_img['id'],
                                              'w' => $_img['width'],
                                              'h' => $_img['height']
                                            ];
              }
            }
            if(count($imgs) >= 1) {
              $product->images = json_encode($imgs);
            }

            $product->save();
        }


        $this->info('Synced ' . $iteratedProducts . ' out of ' . $totalProductsFromApi . ' products.');
    }
}
