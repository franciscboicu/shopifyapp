<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Selector extends Model
{
    protected $table="selectors";

    public static function getAll() {
        return Selector::all();
    }

    public function getSelectorKey()
    {
        return $this->selector_key;
    }

    public function getSelectorValue()
    {
        return $this->selector_value;
    }

}
