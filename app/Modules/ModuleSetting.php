<?php

namespace App\Modules;

use Illuminate\Database\Eloquent\Model;

class ModuleSetting extends Model
{
    protected $with = ['module']; 

    protected $table="module_settings";

    public function module()
    {
        return $this->belongsTo('App\Modules\Module', 'module_id', 'id');
    }

    public function isSelectBox() {
      if($this->s_options === 'false'){
        return false;
      }

      return true;
    }

    public function isTextBox() {
      if($this->isSelectBox() == false && $this->isColorPicker() == false){
        return true;
      }
      return false;
    }

    public function isColorPicker() {
      if($this->is_color_picker == true){
        return true;
      }

      return false;
    }

    public function getSelectOptions(){

      if($this->isSelectBox() == true){
        return explode(",", $this->s_options);
      }

      return null;
    }

}
