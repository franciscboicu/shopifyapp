<?php

namespace App\Modules;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'modules';

    public function getRef(){
        return str_replace('-', '', $this->slug);
    }
}
