<?php

namespace App\Modules;

use Illuminate\Database\Eloquent\Model;

class StoreModuleSetting extends Model
{
    protected $table = 'store_module_settings';
    protected $guarded = ['id'];

    public function moduleSettings()
    {
        //->where('is_enabled', 1)
        return $this->hasOne('App\Modules\ModuleSetting', 's_key', 's_key');
    }

    public function settings()
    {
        //->where('is_enabled', 1)
        return $this->hasOne('App\Modules\ModuleSetting', 's_key', 's_key');
    }

    public function getSKey(){
        return $this->s_key;
    }

    public function getSValue(){
        return $this->s_value;
    }

    public function setSValue($value){
        $this->s_value = $value;
    }

    public static function getModuleSettings($module, $store) {
        $storeModuleSettings = StoreModuleSetting::with('settings')->where('module_id', $module->id)->where('store_id', $store->id)->get();

        if(!$storeModuleSettings){
            return [];
        }

        $_return = [];

        foreach($storeModuleSettings as $storeModuleSetting) {
            $_return[$storeModuleSetting->getSKey()] = $storeModuleSetting;
        }

        return $_return;
    }

    /**
     * Get all settings of all active modules 
     * @param Store @store
     * return array
     */
    public static function getAll($store) {
		$storeModules = StoreModule::where('store_id', $store->id)->where('enabled', 1)->pluck('module_id')->toArray();

        if(!$storeModules){
            return [];
        }

		$storeModuleSettings = StoreModuleSetting::with('settings')->where('store_id', $store->id)->whereIn('module_id', $storeModules)->get();
        
        if(!$storeModuleSettings){
            return [];
        }

        $_settings = [];
        
        foreach($storeModuleSettings as $storeModuleSetting) {
			$_settings[$storeModuleSetting->settings->module->getRef()][$storeModuleSetting->getSKey()] = $storeModuleSetting->getSValue();
        }

        return $_settings;
    }
}
