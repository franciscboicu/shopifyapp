<?php

namespace App\Modules;

use Illuminate\Database\Eloquent\Model;

class StoreModule extends Model
{
    protected $table = 'store_modules';
    public function module()
    {
        return $this->hasOne('App\Modules\Module', 'id', 'module_id')->where('is_enabled', 1);
    }

    public function toggleOnOff($autosave = false){
        $this->enabled = (0 == $this->enabled) ? 1: 0;

        if($autosave){
            $this->save();
        }
    }

}
