<?php

namespace App;

use Carbon\Carbon;
use ShopifyBilling;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{

    protected $fillable = [
        'name', 'domain'
    ];

    public function users()
    {
        return $this->belongsToMany('App\User', 'store_users');
    }

    public function modules()
    {
        //->where('enabled', 1)
        return $this->hasMany('App\Modules\StoreModule')->with('module')->orderBy('order', 'ASC');
    }

    public function onTrial($subscription = 'default', $plan = null)
    {
        if (func_num_args() === 0 && $this->onGenericTrial()) {
            return true;
        }

        $subscription = $this->subscription($subscription);

        if (is_null($plan)) {
            return $subscription && $subscription->onTrial();
        }

        return $subscription && $subscription->onTrial() &&
        $subscription->stripe_plan === $plan;
    }

    public function onGenericTrial()
    {
        return $this->trial_ends_at && Carbon::now()->lt($this->trial_ends_at);
    }

    public function subscribed($subscription = 'default', $plan = null)
    {
        $subscription = $this->subscription($subscription);

        if (is_null($subscription)) {
            return false;
        }

        if (is_null($plan)) {
            return $subscription->valid();
        }

        return $subscription->valid() &&
        $subscription->shopify_plan === $plan;
    }


    public function subscription($subscription = 'default')
    {
        return $this->subscriptions->sortByDesc(function ($value) {
            return $value->created_at->getTimestamp();
        })->first(function ($value) use ($subscription) {
            return $value->name === $subscription;
        });
    }


    public function subscriptions()
    {
        return $this->hasMany('App\Charge')
            ->where('charge_type', Charge::CHARGE_RECURRING)
            ->orderBy('created_at', 'desc');
    }

    public function updateStoreDetailsFromApi(User $user){

    }

}
