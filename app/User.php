<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function stores()
    {
        return $this->belongsToMany('App\Store', 'store_users');
    }

    public function providers()
    {
        return $this->hasMany('App\UserProvider');
    }

    public function provider()
    {
      return $this->providers()->first();
    }

}
