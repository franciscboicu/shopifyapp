<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $guarded = ['id'];

    public function getAllImages() {
      return json_decode($this->images, true) ?? NULL;
    }

    public function getAllTags() {
      if("" === $this->tags) {
        return null;
      }

      $_tags = explode(",", $this->tags);

      if(!is_array($_tags)){
        return [$this->tag];
      }

      $returnTags = [];
      foreach($_tags as $_tag) {
        $returnTags = $_tag;
      }

      return $returnTags;
    }
}
