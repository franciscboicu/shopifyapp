<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleStoreData extends Model
{
    protected $table = 'module_store_data';
    protected $guarded = ['id'];

    const DYNAMIC_TITLE_RETURN_COUNT = 'dynamicTitleReturnCount';

    public static function updateSingleRecord($storeId, $moduleId, $dataType, $data) {

    }

    public static function getDataKey($store, $key){
      return ModuleStoreData::where('store_id', $store->id)->where('data_key', $key)->first();
    }

    public function getValue(){
      if('integer' === $this->data_type){
        return $this->data_integer;
      }
      if('string' === $this->data_type){
        return $this->data_string;
      }

      return 'N/A';
    }

    public static function getReturnsFromDynamicTitle($store){
      return self::getDataKey($store, self::DYNAMIC_TITLE_RETURN_COUNT);
    }


}
