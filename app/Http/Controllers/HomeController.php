<?php

namespace App\Http\Controllers;

use App\Store;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Cache;
use Shopify;
use PHPShopify\ShopifySDK;
use App\ModuleStoreData;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $userStores = auth()->user()->stores;

        return view('home', [
            'userStores' => $userStores,
        ]);
    }

    public function dashboard() {

      mail("franciscboicu@gmail.com","Test Subject","test message");

      $userStores = auth()->user()->stores;

      if(!$userStores) {
          die('No stores installed for your user. Please contact tech support!');
      }

      $returnsFromDynamicTitle = ModuleStoreData::getReturnsFromDynamicTitle($userStores->first());
      if(!$returnsFromDynamicTitle) {
        $returnsFromDynamicTitle = 'N/A';
      } else {
        $returnsFromDynamicTitle = $returnsFromDynamicTitle->getValue();
      }
      return view('admin.dashboard', [
        'returnsFromDynamicTitle' => $returnsFromDynamicTitle,
        'userStores' => $userStores,
      ]);

    }

    public function installScriptTags(Request $request) {

        $store = auth()->user()->stores()->first();

        if( NULL !== $store->script_tags_instaled) {
            die('Shop already has Script Tags Installed');
        }

        $provider = Auth::user()->provider();

        $config = array(
            'ShopUrl' => $store->domain,
            'AccessToken' => $provider->provider_token,
        );

        $shopifyApi = ShopifySDK::config($config);

        $storeKey = md5($store->id.'-'.$store->domain .'-'.'key-control');

        $store->store_key = $storeKey;
        $store->save();

        $scriptTagsData = [
            'event' => "onload",
            'src'   => "https://shopifyapp.dev/js/".$store->id."/".$store->store_key
        ];

        $scriptTagReturn = $shopifyApi->ScriptTag->post($scriptTagsData);

        $store->script_tags_installed = $scriptTagReturn['id'];

        if(!isset($scriptTagReturn['id'])) {
            die('Please contact support. Something went wrong with the Shopify Request');
        }

        $store->save();

        return redirect(route('admin/dashboard'));
    }

}
