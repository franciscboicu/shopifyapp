<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use App\Selector;
use Shopify;
use PHPShopify\ShopifySDK;
use Auth;
class ManagerController extends Controller
{
    public function dashboard(){

      // $provider = Auth::user()->provider();

      // $config = array(
      //   'ShopUrl' => 'voninoninonino.myshopify.com',
      //     'AccessToken' => $provider->provider_token,
      // );

      // $shopifyApi = ShopifySDK::config($config);

      // $products = $shopifyApi->Product->get();


        $stores = Store::all(); 
        
        $selectors = Selector::getAll();

        return view('manager.index',[
            'stores'        =>  $stores,
        ]);
    }

    public function createModule(Request $request){

    }

    public function listShops(Request $request){

    }

    public function finances(Request $request){

    }

}
