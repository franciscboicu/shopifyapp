<?php

namespace App\Http\Controllers;

use App\Store;
use App\Product;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Cache;
use Shopify;
use PHPShopify\ShopifySDK;

class ProductController extends Controller
{
  // $provider = Auth::user()->provider();
  //
  // $config = array(
  //   'ShopUrl' => 'time-traveling-machines.myshopify.com',
  //     'AccessToken' => $provider->provider_token,
  // );
  //
  // $shopifyApi = ShopifySDK::config($config);
  //
  // $products = $shopifyApi->Product->get();
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $store = Auth::user()->stores()->first();

        $products = Product::where('store_id', $store->id)->get();


        return view('admin.products', [
            'store' => $store,
            'products' => $products,
        ]);
    }

    public function dashboard() {



    }

}
