<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use App\Selector;
use Shopify;
use PHPShopify\ShopifySDK;
use Auth;

class SelectorController extends Controller
{

    public function __construct() {

    }

    public function dashboard(){

        if(Auth::user()->is_root != true){
            die('NO!');
        }

        $selectors = Selector::getAll();

        return view('manager.selectors',[
            'selectors'        =>  $selectors,
        ]);
    }

    public function create(Request $request) {

    }

}
