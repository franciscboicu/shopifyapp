<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Module;
use App\Modules\StoreModule;
use App\Modules\ModuleSetting;
use App\Modules\StoreModuleSetting;
use Auth;
use View;
use Illuminate\Support\Facades\Redis;


class ModuleController extends Controller
{
    public function index(Request $request, $slug = NULL)
    {
      // $testRedis = Redis::get('keyX.'.$slug);
      //
      // if($testRedis == NULL) {
      //   print"Notset";
      //   $testRedis = Redis::set('keyX.'.$slug, 'SomeValue');
      // }
      //
      // print "<br/>";
      // print_r($testRedis);
      //
      // exit();
      $store = Auth::user()->stores()->first();

      //take the global module and global module settings
      $module = Module::where('slug', $slug)->first();
      $storeModule = StoreModule::where('store_id', $store->id)->where('module_id', $module->id)->first();

      //take the store module settings
      $storeModuleSettings = StoreModuleSetting::getModuleSettings($module, $store);

      //if there are no settings go and update them and redirect to self
      if (count($storeModuleSettings) == 0) {
        $moduleSettings = ModuleSetting::where('module_id', $module->id)->get();
        $this->checkSettingsAndUpdate($store, $module, $moduleSettings, $storeModuleSettings);
        return redirect(route('admin/module/view',['slug'=>$module->slug]));
      }

      $viewName = (View::exists('admin.modules.'.$module->slug)) ? 'admin.modules.'.$module->slug: 'admin.module';

    	return view($viewName,[
        'module'        => $module,
        'storeModuleSettings' => $storeModuleSettings,
        'store' => $store,
        'storeModule' => $storeModule
      ]);
    }

    public function updateSetting(Request $request){

      $store = Auth::user()->stores()->first();

      //load the setting by id, store id and s_key
      $storeModuleSetting = StoreModuleSetting::where('id', $request->get('store_module_setting_id'))
                                                ->where('s_key', $request->get('s_key'))
                                                ->where('store_id', $store->id)
                                                ->first();

      if (!$storeModuleSetting) {
        return response()->json([
          'error' => 'This setting could not be found',
          'success' => false
          ]);
      }

      //validate s_value with the global module settings
      //validate if string, integer, colorpicker("string"), or select box is allowed to use option

      $storeModuleSetting->setSValue($request->get('s_value'));
      $storeModuleSetting->save();

      return response()->json([
              'data' => [$request->get('s_key'),$request->get('s_value')],
              'success' => true
              ]);
    }

    //move this to a service or class to perform the update
    //update it so it will also make the differences.if a new global module setting is available
    //to put it here
    private function checkSettingsAndUpdate($store, $module, $moduleSettings, $storeModuleSettings) {

      foreach ($moduleSettings as $moduleSetting) {
        StoreModuleSetting::create([
          'store_id' => $store->id,
          'module_id' => $module->id,
          's_key' => $moduleSetting->s_key,
          's_value' => $moduleSetting->s_value,
        ]);
      }

    }

    //change the status of a module, from on to off or viceversa
    //by doing a toggle
    public function changeStatus(Request $request, $slug = null, $redirectTo = 'slug')
    {

      $store = Auth::user()->stores()->first();
      $module = Module::where('slug', $slug)->first();
      $storeModule = StoreModule::where('module_id', $module->id)->where('store_id', $store->id)->first();

      if (!$storeModule) {
        die('This module does not exist.');
      }

      $storeModule->toggleOnOff($autosave = true);

      if('slug' === $redirectTo) {
        return redirect(route('admin/module/view', ['slug' => $module->slug ]));
      }

      return redirect(route('admin/dashboard'));

    }

    //first code written with my new keyboard
    public function myNewKeyboard(){

    }
}
