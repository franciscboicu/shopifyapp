<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use View;
use App\Store;
use App\Modules\StoreModuleSetting;
use App\Modules\StoreModule;
use App\StoreSelector;
use App\ModuleStoreData;

class PublicController extends Controller
{

	/**
	 * Returns the details of a store, based on the paths of the Shopify API
	 *
	 */
	private function getStoreDetails($store){
		return [
			'storeId' 			=> $store->id,
			'name' 					=> $store->name,
			'domainUrl' 		=> "https://" . $store->domain,
			'domain' 				=> $store->domain,
			'cartUrl' 			=> "https://" . $store->domain . '/cart.json',
			'productsUrl' 	=> "https://" . $store->domain . '/products.json',
		];
	}

	private function getAppDetails($request){
		return [
			'referer' 		=> $request->headers->get('referer'),
			'serverTime' 	=> time(),
			'apiBasePath' 	=> 'https://shopifyapp.dev/api/'
		];
	}

	/** Some other Test */
	public function theJs($jsFile){
		//55c67a6
		$file = '55c67a65.js';
		if(file_exists('js/'.$file)){
			$fileContent = file_get_contents('js/'.$file);
			return response($fileContent, 200)
                  ->header('Content-Type', 'text/javascript');
			exit();
		}

		print "File does not exist";
	}


	/** Test */
	public function jsFile(Request $request, $storeId, $storeKey) {
		$path = resource_path('thejs.js');
		$jsFile = file_get_contents($path);
		print $jsFile;
	}


	/**
	 * Returns the JS that is embeded in the Shopify App
	 */
  public function js(Request $request, $storeId, $storeKey){

	  	//test for allowed server requesters
		$store = Store::where('store_key', $storeKey)->where('id', $storeId)->first();

		$data['store'] = $this->getStoreDetails($store);
		$data['server']   = $this->getAppDetails($request);


		if ( !$store || !isset($_GET['shop']) || ($store->domain !== $_GET['shop']) ) {
	   		return 'console.log("There is no dark side in the moon, really. Matter of fact, it\'s all dark. The only thing that makes it look light is the sun.")';
	   	}

		$storeModuleSettings = StoreModuleSetting::getAll($store);
		$storeSelectors = StoreSelector::getStoreSelectors($store);
		$moduleJsFiles = array();

		foreach($storeModuleSettings as $fileName => $storeModuleSetting){
			if(view::exists('embeded-'.$fileName)){
				$moduleJsFiles[] = view('embeded-'.$fileName);
			}
		}

		return response()
			->view('embeded', [
				'moduleSettings' => $storeModuleSettings,
				'moduleJsFiles'  => $moduleJsFiles,
				'storeSelectors' => $storeSelectors,
				'store'          => $data['store'],
				'server'         => $data['server']
			])->header('Content-Type', 'application/json');

    }

  public function api(Request $request) {
  	$return = [];

		$return['referer'] = $request->headers->get('referer');
		$return['ip'] = $request->ip();

		return $this->response($return);
	}

	public function getCart(Request $request) {
		$return['referer'] = $request->headers->get('referer');
		$return['ip'] = $request->ip();

		return $this->response($return);
	}

	/**
	 * Sets up the header of the response
	 */
	private function response($data){
		return response($data)
		->header('Content-Type','application/json')
		->header('supportsCredentials' ,false)
		->header('allowedOrigins', ['*'])
		->header("Access-Control-Allow-Origin", "*")
		->header('allowedHeaders', ['Content-Type', 'X-Requested-With'])
		->header('allowedMethods', ['*'])
		->header('exposedHeaders', [])
		->header('maxAge', 0);
	}


	/**
	 * Helps updating a record. Switches between module types
	 * and calls the proper module method
	 */
	public function updateRecord(Request $request, $storeId, $recordName) {

		$validateRequest = $this->validateRequest($request);

		if ( false === $validateRequest || null === $validateRequest ) {
			return $this->response(['success' => false]);
		}

		if('dynamictitle' == $recordName) {
			return response()->json([
				'success' => $this->updateReturningFromDynamicTitle($storeId)
			]);
		}

	}

	/**
	 * Updates the record of a store, regarding the Dynamic Title return counter
	 */
	private function updateReturningFromDynamicTitle($storeId) {
		$data = ModuleStoreData::firstOrNew([
			'store_id' => $storeId,
			'data_key' => ModuleStoreData::DYNAMIC_TITLE_RETURN_COUNT,
			'data_type' => 'integer'
		]);

		$data->data_integer += 1;
		return $data->save();
	}

	/**
	 * Should validate the request in a way or another
	 * will be a soft validation
	 */
	public function validateRequest($request){
		return true;

	}
}
