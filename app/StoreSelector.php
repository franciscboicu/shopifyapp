<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreSelector extends Model
{
    protected $table = 'store_selectors';

    // protected $with = 'selector';
    // public function selector()
    // {
    //     //->where('enabled', 1)
    //     return $this->belongsTo('App\Selector');
    // }

    public static function getStoreSelectors($store) {

        $storeSelectors = StoreSelector::where('store_id', $store->id)->get();
        if(!$storeSelectors){
            return [];
        }

        $return = [];
        foreach($storeSelectors as $storeSelector) {
            $return[$storeSelector->selector_key] = $storeSelector->selector_value;
        }

        return $return;
    }
}
