Modules
 - id
 - slug
 - name
 - short_description
 - full_description
 - pictures_json
 


Site Modules
 - id
 - site_id
 - enabled (t/f)
 - timestamps



module_settings
 - id
 - module_id
 - s_type
 - s_data
 - s_key
 - s_val
 