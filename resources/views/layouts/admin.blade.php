<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{env('APP_NAME')}}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('admin/css/bootstrap.min.css') }}?v=1" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="{{ asset('admin/css/animate.min.css') }}" rel="stylesheet" />
    <!--  Paper Dashboard core CSS    -->
    <link href="{{ asset('admin/css/paper-dashboard.css') }}" rel="stylesheet" />
    <link href="{{ asset('admin/css/demo.css') }}" rel="stylesheet" />
    <link href="{{ asset('admin/css/dropzone.min.css') }}" rel="stylesheet" />

    <!--  Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli:400,300" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/css/themify-icons.css') }}" rel="stylesheet" />
    <script src="{{ asset('admin/js/jscolor-2.0.5/jscolor.js') }}"></script>

</head>
<body>

<div class="wrapper">
    @include('admin.sidebar')

    <div class="main-panel">
        @include('admin.topbar')

					@yield('content')

        @include('admin.footer')

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script
    src="https://code.jquery.com/jquery-2.2.4.min.js"
    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
    crossorigin="anonymous"></script>



    <script src="{{ asset('admin/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="{{ asset('admin/js/bootstrap-checkbox-radio.js') }}"></script>
	<!--  Charts Plugin -->
  <!--  Notifications Plugin    -->
  <script src="{{ asset('admin/js/bootstrap-notify.js') }}"></script>
  <!--  Google Maps Plugin    -->
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
  <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="{{ asset('admin/js/paper-dashboard.js') }}"></script>
	<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
	<script src="{{ asset('admin/js/demo.js') }}"></script>
	<script src="{{ asset('admin/js/dropzone.js') }}"></script>

<script>

    $(document).ready(function(){

 
    });

</script>


	@yield('jscripts')




</html>
