(function(){

	var initialPageTitle = $("title").text();
	var barTitleMessageA = SETTINGS.dynamictitle.message_1;
	var barTitleMessageB = SETTINGS.dynamictitle.message_2;
	var barTitleMessageC = SETTINGS.dynamictitle.message_3;


	var wnHowManyWatchers = window.wn_how_many;
	var exitedPage = false;
	var localStorage = window.localStorage;
	var titleText = document.title;



	if("Simple" === SETTINGS.dynamictitle.type)
	{

		$(window).blur(function() {
			exitedPage = true;

			$("title").text(barTitleMessageA);

		});

		$(window).focus(function() {

			$("title").text(initialPageTitle);

			if(exitedPage == true) {

				if(localStorage.getItem('wasSynced') === null) {
						localStorage.setItem('wasSynced', 1);
						$.get( SERVER.apiBasePath + 'v1/update/record/'+STORE.storeId+'/dynamictitle',function(response){
								console.log(response);
						});
				}

			}

		});

	}




})()
