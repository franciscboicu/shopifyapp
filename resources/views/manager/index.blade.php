@extends('layouts.admin')


@section('content')
<style>

.statusButton{
    margin-top:-50px;
}
</style>
<div class="content">
    <div class="container-fluid">
       <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-warning text-center">
                                    <i class="ti-server"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Stores</p>
                                    {{count($stores)}}
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-reload"></i> Updated now
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-success text-center">
                                    <i class="ti-wallet"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Revenue</p>
                                    $11000
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-calendar"></i> Last 30 days
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-danger text-center">
                                    <i class="ti-pulse"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Bug Reports</p>
                                    1
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-timer"></i> not solved
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-info text-center">
                                    <i class="ti-twitter-alt"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Reviews</p>
                                    +145
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-reload"></i> 4.7 stars rating
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if($stores)
            @foreach($stores as $store)
            <div class="row">
                <div class="col-md-12">
                    <div class="card" >
                        <div class="header">
                            <h4 class="title">{{$store->name}}</h4>
                            <p class="category">
                                <a href="https://{{$store->domain}}" target="_blank">{{$store->domain}}</a>
                            </p>

                            <a href="#" class="btn btn-success pull-right statusButton" style="">ACTIVE APP</a>

                        </div>
                        <div class="content">

                            <div class="footer">

                                <hr>
                                <div class="stats">
                                    <i class="ti-money"></i> Payed <strong>80$</strong> in the last <i>8 months</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        @endif

    </div>
</div>


@endsection

@section('jscripts')
<script>

var myGradesCalculate = (function (test) {
    
    var myGrades = [93, 95, 88, 0, 55, 91];
    console.log(test);
    return {
      average: function() {
        var total = (function(param) {
          return param;
          });
          
        return 'Your param is ' + total + '.';
      },
  
      ups: function(something) {
        
        return 'You hey!' + something + ' yupiii.';
      }
    }
  })('happy');
  
  console.log(myGradesCalculate.ups('tetetetetetetetete YEY. SUCCESS')); // 'You failed 2 times.' 


</script>
@endsection