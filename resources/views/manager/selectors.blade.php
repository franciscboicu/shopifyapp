@extends('layouts.admin')


@section('content')
<style>

.statusButton{
    margin-top:-50px;
}

<style>

.statusButton{
    margin-top:-50px;
}

.p-10{
  padding:10px;
}

small{
  font-size:12px;
}
.input{
  border:1px solid #ccc;
}

 .select{
   border:1px solid #ccc;
 }

</style>
<div class="content">
    <div class="container-fluid">

            <div class="row">
                    <div class="col-md-12">
                        <div class="card" >
                            <div class="header">
                                <h4 class="title">Selectors & Global Settings</h4>
                                <p class="category">
                                    <a href="" target="_blank"></a>Global Settings with DOM Selectors, that are added per store, in case of custom themes
                                </p>
    
                                <a href="#" class="btn btn-success pull-right statusButton" style="">Add new Selector</a>
    
                            </div>
                            <div class="content">
    
                                <div class="footer">
    
                                    <hr>
                                    <div class="stats">
                                        {{-- <i class="ti-money"></i> Payed <strong>80$</strong> in the last <i>8 months</i> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        @if($selectors)
            @foreach($selectors as $selector)
            <div class="row">
                <div class="col-md-12">
                    <div class="card" >
                        <div class="header">
                            <h4 class="title">{{$selector->label}}</h4>
                            <p class="category">
                                {{$selector->description}}
                            </p>


                            <button href="#" class="btn btn-success pull-right statusButton" style="">Sync to all stores</button>

                            <div class="row">
                                <div class="col-md-6">KEY</div>
                                <div class="col-md-6">VALUE</div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">

                                        <input class="form-control editable input" 
                                        name="" 
                                        value="{{$selector->getSelectorKey()}}"
                                        placehold=""  
                                        data-store-module-setting-s-key="" 
                                        data-store-module-setting-id=""
                                        />

                                </div>
                                <div class="col-md-6">

                                        <input class="form-control editable input" 
                                        name="" 
                                        value="{{$selector->getSelectorValue()}}"
                                        placehold=""  
                                        data-store-module-setting-s-key="" 
                                        data-store-module-setting-id=""
                                        />

                                </div>
                            </div>
                        </div>
                        <div class="content">

                            <div class="footer">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        @endif

    </div>
</div>


@endsection

@section('jscripts')
<script>

var myGradesCalculate = (function (test) {
    
    var myGrades = [93, 95, 88, 0, 55, 91];
    console.log(test);
    return {
      average: function() {
        var total = (function(param) {
          return param;
          });
          
        return 'Your param is ' + total + '.';
      },
  
      ups: function(something) {
        
        return 'You hey!' + something + ' yupiii.';
      }
    }
  })('happy');
  
  console.log(myGradesCalculate.ups('tetetetetetetetete YEY. SUCCESS')); // 'You failed 2 times.' 


</script>
@endsection