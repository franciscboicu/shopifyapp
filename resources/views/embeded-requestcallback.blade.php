function RequestCallback(SELECTORS) {

	var modalHTML = `<div id="cb_modal" class="cb_modal">\
		<div class="cb_modal-content">\
			<div class="cb_modal-header">\
			<span class="cb_close">&times;</span>\
				<h2 id="rc_popup_title">Popup Text</h2>\
			</div>\
			<div class="cb_modal-body">\
				<p>Some text in the Modal Body</p>\
				<p>Some other text...</p>\
				<p>Some text in the Modal Body</p>\
				<p>Some other text...</p>\
				<p>Some text in the Modal Body</p>\
				<p>Some other text...</p>\
				<p>Some other text...</p>\
				<p>Some other text...</p>\
				<p>Some other text...</p>\
				<p>Some other text...</p>\
			</div>\
			
		</div>\
		</div>`;

	var buttonHTML=`<div class="demo wiggle " id="request_callback_button">\
					`+SETTINGS.requestcallback.rc_bubble_text+`
					</div>`;

	//append modal and button
	$("body").append(modalHTML);
	$("body").append(buttonHTML);
	
	var buttonPosition = SETTINGS.requestcallback.rc_bubble_position;
	$("#request_callback_button").addClass(buttonPosition);				
	var modal = document.getElementById("cb_modal");
	
	//show the popup by changing the property to block
	$("#request_callback_button").click(function(){
		modal.style.display = "block";
	});

	// Get the button that opens the modal
	var btn = document.getElementById("request_callback_button");

	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("cb_close")[0];


	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
	}							
}

//init the module
RequestCallback(SELECTORS);
