function staticBar() {

	var css = '.sbar { padding: 5px; z-index:1000;  font-size: ' + SETTINGS.staticbar.font_size + 'px; font-family: ' + SETTINGS.staticbar.font_family + ';background-color: #' + SETTINGS.staticbar.background_color + '; color: #' + SETTINGS.staticbar.color + '; min-height: ' + SETTINGS.staticbar.bar_height + 'px;}',

	head = document.head || document.getElementsByTagName('head')[0],
    style = document.createElement('style');
	head.appendChild(style);
	style.type = 'text/css';
	if (style.styleSheet) {
	// This is required for IE8 and below.
	style.styleSheet.cssText = css;
	} else {
	style.appendChild(document.createTextNode(css));
	}

	var bar = '<div class="s__bar sbar" id="#staticbar"><span class="s__bar_content">'+SETTINGS.staticbar.display_text+'</span></div>';

	$("body").prepend(bar);

	$("#staticbar").css("background-color", "red");
}

staticBar();
