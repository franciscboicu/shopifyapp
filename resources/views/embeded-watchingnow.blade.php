function WatchingNow(SELECTORS) {

	if(!SELECTORS.PRODUCT_TITLE){
		return;
	}

	var watchersCSS = `<style>\
	.watchers_block {\
		!important;
		display:block;\
		padding:6px;\
		width:100%;\
		font-weight:bold;\
	}\
	<style>`;

	$(watchersCSS).insertBefore($(SELECTORS.PRODUCT_TITLE));

	var watchersHTMLElement = `<div id="watchers" class="watchers_block">\
		<p id="wt">🔥🔥🔥 #NUMBER_OF_WATCHERS# people are watching this product</p></div>`;


	text = watchersHTMLElement.replace("#NUMBER_OF_WATCHERS#", '<span id="wn_number"></span>');


	$(text).insertAfter($(SELECTORS.PRODUCT_TITLE));

	$('#watchers').css('background-color', '#' + SETTINGS.watchingnow.wn_background_color );
	$('#wt').css('color', '#' + SETTINGS.watchingnow.wn_text_color );
	$('#watchers').css('border-style', '#' + SETTINGS.watchingnow.wn_border_style);
	$('#watchers').css('border-color', '#' + SETTINGS.watchingnow.wn_border_color);

	NO = Math.floor((Math.random() * SETTINGS.watchingnow.wn_max_watchers) + SETTINGS.watchingnow.wn_min_watchers);

	$("#wn_number").text(NO);

	window.wn_how_many = NO;

	setInterval(function(){

		highOrLow = Math.floor((Math.random() * 2) + 1);

		currentWatchersNumber = parseInt($("#wn_number").text());

		if(highOrLow == 1){
			newWatchersNumber = currentWatchersNumber - 1;
		} else {
			newWatchersNumber = currentWatchersNumber + 1;
		}

		if(newWatchersNumber < SETTINGS.watchingnow.wn_min_watchers) {
			newWatchersNumber++;
		}

		if(newWatchersNumber > SETTINGS.watchingnow.wn_max_watchers) {
			newWatchersNumber--;
		}

		$("#wn_number").text(newWatchersNumber);

		window.wn_how_many = newWatchersNumber;

	}, SETTINGS.watchingnow.wn_update_interval * 1000);



}

WatchingNow(SELECTORS);
