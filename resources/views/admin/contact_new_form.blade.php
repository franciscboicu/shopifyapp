<?php use App\Contact; ?>

<div class="card hidden" id="formNewContact">

  <div class="header">
      <h4 class="title">Contact nou</h4>
      <p class="pull-right" style="margin-top:-37px;">
        <span class="ti-close" id="closeAddNewContactForm"></span>
      </p>
  </div>

  <div class="content ">

<div class="row">
  <div class="col-lg-12" style="text-align:center;">
    <form action="{{route('contact/save')}}" method="POST" id="save">
    {{ csrf_field() }}

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label class="pull-left">Tip Contact</label>
                <select class="form-control" name="contact_type">
                  @foreach(Contact::TYPES as $contactTypeId => $contactTypeLabel)
                  <option value="{{$contactTypeId}}">{{$contactTypeLabel}}</option>
                  @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="pull-left">Nume</label>
                <input type="text"  name="name" class="form-control border-input" placeholder="Titlu" value="">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="pull-left">Telefon</label>
                <input type="text"  name="phone_numbers" class="form-control border-input" placeholder="Titlu" value="">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="pull-left">Email</label>
                <input type="text"  name="email" class="form-control border-input" placeholder="Titlu" value="">
            </div>
        </div>
    </div>
    <div class="text-center">
        <button type="submit"  class="btn btn-info btn-fill btn-wd">Continua</button>
    </div>
    <div class="clearfix"></div>
</form>


</div>
</div>



  </div>
</div>
