@section('jscripts')
<script>



$(document).ready(function(){



//general function - TO BE MOVED
function notifySuccess(message){
  notify = $.notify({
      icon: 'ti-success',
      message: message
    },{
        type: 'success',
        timer: 1000
    });
}
//general function - TO BE MOVED
function notifyError(message){
  notify = $.notify({
      icon: 'ti-danger',
      message: message
    },{
        type: 'danger',
        timer: 1000
    });
};  

$('.editable').keypress(function(){
  $(this).closest('.content').find('.cta-save').show();
});

/**
 * Sends a request to update the value of a filter
 * @var Event
 */
$('.editable').change(function(e){
  e.preventDefault();

  var store_module_setting_id = $(this).data('store-module-setting-id');
  var s_key = $(this).data('store-module-setting-s-key');
  var s_value = $(this).val();

  $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: "POST",
      url: "{{route('admin/module/setting/update')}}",
      data: {'s_key': s_key, 's_value': s_value, 'store_module_setting_id': store_module_setting_id},
      success: function(response) {
        if(response.success == true){
          $(this).closest('progress').attr('value', 100);
          notifySuccess('The setting was updated!');
        } else {
          notifyError(response.error);
        }
        $(this).closest('.content').find('.cta-save').css("display", "none");

      },
      error: function() {
        notifyError('The Request went wrong!');
        $(this).closest('.content').find('.cta-save').css("display", "none");
      }
  });


});


});

</script>
@endsection