<div class="row">
    <div class="col-md-12">
        <div class="card" >
            <div class="header">
                <h4 class="title"><a href=""><strong>{{$module->name}}</strong></a></h4>
                <p class="category">
                    {{$module->short_description}}
                </p>
            </div>
            <div class="content">

                  @if($storeModule->enabled == 1)
                  <a href="{{route('admin/module/changestatus', ['slug' => $module->slug ])}}" class="btn btn-success pull-right statusButton" style="">ONLINE</a>
                  @else
                  <a href="{{route('admin/module/changestatus', ['slug' => $module->slug ])}}" class="btn btn-default pull-right statusButton" style="">OFFLINE</a>
                  @endif

            </div>
        </div>
    </div>
</div>
