<div class="sidebar" data-background-color="black" data-active-color="danger">

<!--
Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
-->

  <div class="sidebar-wrapper">
        <div class="logo">
            <a href="" class="simple-text">
                {{env('APP_NAME')}}
            </a>
        </div>

        <ul class="nav">
            
            <li @if(Route::is('admin/dashboard')) class="active" @endif>
                <a href="{{route('admin/dashboard')}}">
                    <i class="ti-panel"></i>
                    <p>Dashboard</p>
                </a>
            </li>

            <li @if(Route::is('admin/products')) class="active" @endif>
                <a href="{{route('admin/products')}}">
                    <i class="ti-rss"></i>
                    <p>Products</p>
                </a>
            </li>


            @if(Auth::user()->is_root)
            <li @if(Route::is('manager/dashboard')) class="active" @endif>
                <a href="{{ route('manager/dashboard')}}">
                  <i class="ti-settings"></i>
                    <p>Manager</p>
                </a>
            </li> 
            <li @if(Route::is('manager/selectors')) class="active" @endif>
                <a href="{{ route('manager/selectors')}}">
                  <i class="ti-settings"></i>
                    <p>Selectors</p>
                </a>
            </li> 

            @endif

          <li class="active-pro">
              <a href="#">
                  <i style="color:red;" class="ti-export"></i>
                  <p style="color:red;">Upgrade to PRO</p>
              </a>
          </li>

        </ul>
  </div>
</div>
