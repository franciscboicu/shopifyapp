@extends('layouts.admin')

@section('content')
<?php use App\Property; ?>


<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-12">
  <div class="card">
    <div class="header">
        <h4 class="title">Lista proprietatilor</h4>
        <p class="category">din portofoliul tau</p>
        <p class="pull-right" style="margin-top:-37px;">
          <a href="{{ route('property/new') }}" class="btn btn-success btn-fill btn-wd">Adauga proprietate</a>
        </p>
    </div>
    <div class="content table-responsive table-full-width">

        @if(count($properties) >= 1)
        <table class="table table-striped">
            <thead>
              <th>ID</th>
              <th>Categorie</th>
              <th>Tip tranzactie</th>
              <th>Titlu</th>
              <th>Actiuni</th>
            </thead>
            <tbody>
                @foreach($properties as $property)
                <tr>
                  <td>{{ $property->id }}</td>
                  <td>{{ Property::CATEGORY[$property->category]}}</td>
                  <td>{{ Property::TRANS[$property->transaction]}}</td>
                  <td>{{ $property->title }}</td>
                  <td>
                    <a href="{{ route('property/edit/filters', ['id'=>$property->id]) }}">edit</a>
                    <a href="{{ route('property/delete', ['id'=>$property->id]) }}" onclick="return confirm('Datele nu vor putea fi recuperate. Sunteti sigur?')">delete</a>
                  </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="content">
            {{ $properties->links() }}
        </div>
        @endif



    </div>
  </div>

</div>
</div>
</div>
</div>

@endsection
