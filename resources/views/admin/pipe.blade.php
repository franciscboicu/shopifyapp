@extends('layouts.admin')

@section('content')
<?php use App\Contact; ?>
<style>
.flashOn{
  background-color: yellow;
}
</style>
<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-12">



  <div class="card">
    <div class="header">
        <h4 class="title">Sales Pipe</h4>

        <p class="category">prospectari, telefoane, intalniri, vizionari</p>
        <p class="pull-right" style="margin-top:-37px;">
          <button class="btn btn-success btn-fill btn-wd" id="toggleShowNewActivity">Adauga activitate</button>
        </p>
    </div>
    <div class="content table-responsive">


      <div class="content">
        <div class="row">
          @include("admin.pipe-col", ['stage' => '0', 'tab_color' => '#8792a3', 'tab_name' => 'Hope'])
          @include("admin.pipe-col", ['stage' => '1', 'tab_color' => '#82c6d6', 'tab_name' => 'Horizon'])
          @include("admin.pipe-col", ['stage' => '2', 'tab_color' => '#e28726', 'tab_name' => 'Interested'])
          @include("admin.pipe-col", ['stage' => '3', 'tab_color' => '#4286f4', 'tab_name' => 'Closed'])

        </div>
      </div>


    </div>
  </div>

</div>
</div>
</div>
</div>



@endsection
@section('jscripts')
<script>

$(document).ready(function(){
  $(".stageDetails0").hide();
  $(".stageDetails1").hide();
  $(".stageDetails2").hide();
  $(".stageDetails3").hide();
  $(".stageShowDetails").css('cursor', 'pointer');
  var elem;

  // $(".sidebar").hide();
  // $(".main-panel").css('width', '100%');

  $(".stageContact").click(function(){
    var currentStage = $(this).data('stage');
    var nextStage = currentStage+1;
    if(nextStage>3){return;}
    elem = $(this).parent().closest('li').clone(false,true);
    $(this).closest('li').remove();
    $("#stageList"+nextStage).prepend(elem);
    elem.children('.stageContact').data('stage', nextStage);

    // $('.stageList0').append(elem);
    //

  });

  function unflash(elem) {
      $(elem).removeClass("flashOn");
  }

  $(".stageShowDetails").click(function(){
    var currentStage = $(this).data('stage');
    x = $('.stageDetails'+currentStage).slideToggle();

    if($(this).hasClass('ti-angle-down')){
      $(this).removeClass('ti-angle-down');
      $(this).addClass('ti-angle-up');
    }
    // console.log(x);
  });
});

$('#toggleShowNewActivity').click(function(){
  $('#formNewActivity').removeClass('hidden');
  $(this).addClass('hidden');
});

$('#closeAddNewActivityForm').click(function(){
  $('#toggleShowNewActivity').removeClass('hidden');
  $('#formNewActivity').addClass('hidden');
});

$('#closeAddNewActivityForm').css('cursor', 'pointer');

$('#save').on('submit', function(e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: "{{route('activity/save')}}",
        data: $(this).serialize(),
        success: function(response) {
          if(response.success == true){
            if(response.is_redirect == true){
              window.location.href = response.redirect_url;
            }
          }else{
            alert(false);
          }
        }
    });
});

</script>
@endsection
