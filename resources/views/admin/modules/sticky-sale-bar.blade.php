@extends('layouts.admin')


@section('content')
<style>

.statusButton{
    margin-top:-50px;
}

.p-10{
  padding:10px;
}

small{
  font-size:12px;
}
.input{
  border:1px solid #ccc;
}

 .select{
   border:1px solid #ccc;
 }
</style>
  <div class="content">

    <div class="container-fluid">
            @include('admin.partials.module-info-bar')
    </div>

    <div class="container-fluid">

      @foreach($storeModuleSettings as $moduleSetting)
      <div class="row">

        <div class="col-md-9">
            <div class="card" >
                <div class="content">
 
                  <label for="{{$moduleSetting->s_key}}">{{$moduleSetting->moduleSettings->label}}</label>
                  <label class="pull-right cta-save" style="display:none;"><small>Press enter to save</small></label>

                  @if($moduleSetting->moduleSettings->isSelectBox())
                    <select class="form-control editable select" 
                            data-store-module-setting-s-key="{{$moduleSetting->s_key}}" 
                            data-store-module-setting-id="{{$moduleSetting->id}}">
                      @foreach($moduleSetting->moduleSettings->getSelectOptions() as $selectOption)
                      <option> {{$selectOption}} </option>
                      @endforeach
                    </select>
                  @endif

                  @if($moduleSetting->moduleSettings->isTextBox())
                    <input class="form-control editable input" 
                          name="{{$moduleSetting->s_key}}" 
                          placehold="123131231"  
                          data-store-module-setting-s-key="{{$moduleSetting->s_key}}" 
                          data-store-module-setting-id="{{$moduleSetting->id}}"
                    />
                  @endif

                  @if($moduleSetting->moduleSettings->isColorPicker())
                    <input class="form-control editable input jscolor" 
                          name="{{$moduleSetting->s_key}}"
                          value="{{$moduleSetting->s_value}}"
                          data-store-module-setting-s-key="{{$moduleSetting->s_key}}" 
                          data-store-module-setting-id="{{$moduleSetting->id}}"
                          placehold="123131231" />
                  @endif

                </div>
            </div>
        </div>

        <div class="col-md-3 p-10">
          <p class="text-gray  p-10"><small>{{$moduleSetting->moduleSettings->description}}</small></p>
        </div>

      </div>
      @endforeach

    </div>

</div>



@endsection

@include('admin.partials.scripts')
