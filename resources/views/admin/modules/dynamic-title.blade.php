@extends('layouts.admin')


@section('content')
<style>

.statusButton{
    margin-top:-50px;
}

.p-10{
  padding:10px;
}

small{
  font-size:12px;
}
.input{
  border:1px solid #ccc;
}

 .select{
   border:1px solid #ccc;
 }
</style>
  <div class="content">

    <div class="container-fluid">
      <div class="row">
              <div class="col-lg-3 col-sm-6">
                  <div class="card">
                      <div class="content">
                          <div class="row">
                              <div class="col-xs-5">
                                  <div class="icon-big icon-warning text-center">
                                      <i class="ti-server"></i>
                                  </div>
                              </div>
                              <div class="col-xs-7">
                                  <div class="numbers">
                                      <p>Synced Products</p>
                                      {{count([])}}
                                  </div>
                              </div>
                          </div>
                          <div class="footer">
                              <hr />
                              <div class="stats">
                                  <a href=""><i class="ti-reload"></i> Updated now</a>
                                  <small class="">Last updated: <strong>3 days ago</strong></small>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-lg-3 col-sm-6">
                  <div class="card">
                      <div class="content">
                          <div class="row">
                              <div class="col-xs-5">
                                  <div class="icon-big icon-success text-center">
                                      <i class="ti-wallet"></i>
                                  </div>
                              </div>
                              <div class="col-xs-7">
                                  <div class="numbers">
                                      <p>Returns to page</p>
                                      120
                                  </div>
                              </div>
                          </div>
                          <div class="footer">
                              <hr />
                              <div class="stats">
                                  <i class="ti-calendar"></i> People that returned to store due to dynamic title
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-lg-3 col-sm-6">
                  <div class="card">
                      <div class="content">
                          <div class="row">
                              <div class="col-xs-5">
                                  <div class="icon-big icon-danger text-center">
                                      <i class="ti-pulse"></i>
                                  </div>
                              </div>
                              <div class="col-xs-7">
                                  <div class="numbers">
                                      <p>Support Tickets </p>
                                      <strong  style="color:red">1</strong>/<strong style="color:green">3</strong>
                                  </div>
                              </div>
                          </div>
                          <div class="footer">
                              <hr />
                              <div class="stats">
                                  <button type="" style="position:relative;margin-left:200px;"class="btn btn-info btn-xs btn-fill btn-wd"><i class="ti-help-alt"></i>Open Ticket</button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-lg-3 col-sm-6">
                  <div class="card">
                      <div class="content">
                          <div class="row">
                              <div class="col-xs-5">
                                  <div class="icon-big icon-info text-center">
                                      <i class="ti-twitter-alt"></i>
                                  </div>
                              </div>
                              <div class="col-xs-7">
                                  <div class="numbers">
                                      <p>Reviews</p>
                                      +145
                                  </div>
                              </div>
                          </div>
                          <div class="footer">
                              <hr />
                              <div class="stats">
                                  <i class="ti-reload"></i> 4.7 stars rating
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

            @include('admin.partials.module-info-bar')
    </div>

    <div class="container-fluid">

      @foreach($storeModuleSettings as $moduleSetting)
      <div class="row">

        <div class="col-md-9">
            <div class="card" >
                <div class="content">

                  <label for="{{$moduleSetting->s_key}}">{{$moduleSetting->settings->label}}</label>
                  <label class="pull-right cta-save" style="display:none;"><small>Press enter to save</small></label>

                  @if($moduleSetting->settings->isSelectBox())
                    <select class="form-control editable select"
                            data-store-module-setting-s-key="{{$moduleSetting->s_key}}"
                            data-store-module-setting-id="{{$moduleSetting->id}}">
                      @foreach($moduleSetting->settings->getSelectOptions() as $selectOption)
                        <option @if($selectOption === $moduleSetting->getSValue()) selected @endif> {{$selectOption}} </option>
                      @endforeach
                    </select>
                  @endif

                  @if($moduleSetting->settings->isTextBox())
                    <input class="form-control editable input"
                          name="{{$moduleSetting->s_key}}"
                          placehold="123131231"
                          value="{{$moduleSetting->s_value}}"
                          data-store-module-setting-s-key="{{$moduleSetting->s_key}}"
                          data-store-module-setting-id="{{$moduleSetting->id}}"
                    />
                  @endif

                  @if($moduleSetting->settings->isColorPicker())
                    <input class="form-control editable input jscolor"
                          name="{{$moduleSetting->s_key}}"
                          value="{{$moduleSetting->s_value}}"
                          data-store-module-setting-s-key="{{$moduleSetting->s_key}}"
                          data-store-module-setting-id="{{$moduleSetting->id}}"
                          placehold="123131231" />
                  @endif

                </div>
            </div>
        </div>

        <div class="col-md-3 p-10">
          <p class="text-gray  p-10"><small>{{$moduleSetting->settings->description}}</small></p>
        </div>

      </div>
      @endforeach

    </div>

</div>



@endsection

@include('admin.partials.scripts')
