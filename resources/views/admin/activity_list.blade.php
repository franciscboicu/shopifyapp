@extends('layouts.admin')

@section('content')
<?php use App\Contact; ?>

<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-12">

  @include('admin.activity_new_form')


  <div class="card">
    <div class="header">
        <h4 class="title">Activitati</h4>
        <p class="category">prospectari, telefoane, intalniri, vizionari</p>
        <p class="pull-right" style="margin-top:-37px;">
          <button class="btn btn-success btn-fill btn-wd" id="toggleShowNewActivity">Adauga activitate</button>
        </p>
    </div>
    <div class="content table-responsive table-full-width">

        @if(count($activities) >= 1)
        <table class="table table-striped">
            <thead>
              <th>Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Type</th>
            </thead>
            <tbody>
                @foreach($activities as $activity)
                <tr>
                  <td>{{ $activity->title }}</td>
                  <td>{{ $activity->description }}</td>
                  <td>{{ $activity->starts_at }}</td>
                  <td>{{ $activity->ends_at }}</td>
                  <td>
                    <a href="">edit</a>
                    <a href="" onclick="return confirm('Datele nu vor putea fi recuperate. Sunteti sigur?')">delete</a>
                  </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="content">
            {{ $activities->links() }}
        </div>
        @endif



    </div>
  </div>

</div>
</div>
</div>
</div>

@endsection

@section('jscripts')
<script>


$('#toggleShowNewActivity').click(function(){
  $('#formNewActivity').removeClass('hidden');
  $(this).addClass('hidden');
});

$('#closeAddNewActivityForm').click(function(){
  $('#toggleShowNewActivity').removeClass('hidden');
  $('#formNewActivity').addClass('hidden');
});

$('#closeAddNewActivityForm').css('cursor', 'pointer');

$('#save').on('submit', function(e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: "{{route('activity/save')}}",
        data: $(this).serialize(),
        success: function(response) {
          if(response.success == true){
            if(response.is_redirect == true){
              window.location.href = response.redirect_url;
            }
          }else{
            alert(false);
          }
        }
    });
});

</script>
@endsection
