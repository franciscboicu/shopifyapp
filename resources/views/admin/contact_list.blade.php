@extends('layouts.admin')

@section('content')
<?php use App\Contact; ?>

<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-12">

  @include('admin.contact_new_form')


  <div class="card">
    <div class="header">
        <h4 class="title">Contactele tale</h4>
        <p class="category">parteneri, proprietari, clienti</p>
        <p class="pull-right" style="margin-top:-37px;">
          <button class="btn btn-success btn-fill btn-wd" id="toggleShowNewContact">Adauga contact</button>
        </p>
    </div>
    <div class="content table-responsive table-full-width">

        @if(count($contacts) >= 1)
        <table class="table table-striped">
            <thead>
              <th>Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Type</th>
            </thead>
            <tbody>
                @foreach($contacts as $contact)
                <tr>
                  <td>{{ $contact->name }}</td>
                  <td>{{ $contact->email }}</td>
                  <td>{{ $contact->phone_numbers }}</td>
                  <td>{{ $contact->getType() }}</td>
                  <td>
                    <a href="{{ route('contact/new') }}">edit</a>
                    <a href="{{ route('contact/delete', ['id'=>$contact->id]) }}" onclick="return confirm('Datele nu vor putea fi recuperate. Sunteti sigur?')">delete</a>
                  </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="content">
            {{ $contacts->links() }}
        </div>
        @endif



    </div>
  </div>

</div>
</div>
</div>
</div>

@endsection

@section('jscripts')
<script>

$('#toggleShowNewContact').click(function(){
  $('#formNewContact').removeClass('hidden');
  $(this).addClass('hidden');
});

$('#closeAddNewContactForm').click(function(){
  $('#toggleShowNewContact').removeClass('hidden');
  $('#formNewContact').addClass('hidden');
});

$('#closeAddNewContactForm').css('cursor', 'pointer');

$('#save').on('submit', function(e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: "{{route('contact/save')}}",
        data: $(this).serialize(),
        success: function(response) {
          if(response.success == true){
            if(response.is_redirect == true){
              window.location.href = response.redirect_url;
            }
          }else{
            alert(false);
          }
        }
    });
});

</script>
@endsection
