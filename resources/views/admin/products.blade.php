@extends('layouts.admin')


@section('content')
<style>

.statusButton{
    margin-top:-50px;
}
</style>
<div class="content">
    <div class="container-fluid">

    <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-warning text-center">
                                    <i class="ti-server"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Products</p>
                                    {{count($products)}}
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-reload"></i> Updated now
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-success text-center">
                                    <i class="ti-wallet"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Revenue</p>
                                    $11000
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-calendar"></i> Last 30 days
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-danger text-center">
                                    <i class="ti-pulse"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Bug Reports</p>
                                    1
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-timer"></i> not solved
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-info text-center">
                                    <i class="ti-twitter-alt"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Reviews</p>
                                    +145
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-reload"></i> 4.7 stars rating
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">{{$store->name}}</h4>
                        <p class="category"><a href="https://{{$store->domain}}" target="_blank">  <small>Click to open in new tab</small> </a></p>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>sID</th>
                                <th>Title</th>
                                <th>Handle</th>
                                <th>Tags</th>
                            </tr>
                          </thead>
                            <tbody>
                              @if($products)
                              @foreach($products as $product)
                                <tr>
                                    <td>
                                      <img src="{{ $product->getAllImages()['1']['src'] }}" width="30" height="30" />
                                    </td>
                                    <td>
                                        {{$product->product_id}}
                                    </td>
                                    <td>{{$product->title}}</td>
                                    <td>
                                      <a href="https://{{$store->domain}}/products/{{$product->handle}}" target="_blank">
                                      {{$product->handle}}
                                      </a>
                                    </td>
                                    <td>
                                      {{$product->tags}}
                                    </td>
                                </tr>
                                @endforeach
                              @endif

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('jscripts')


@endsection
