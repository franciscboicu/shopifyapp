@extends('layouts.admin')
<?php use App\Property; ?>
@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">

        <div class="card">

          <div class="header">
              <h4 class="title"> (#{{$property->id}}) {{$property->title}} </h4>

              <p class="pull-right" style="margin-top:-30px;">
                <a href="{{ route('property/edit/filters',['id'=>$property->id]) }}" class="btn btn-xs btn-outline btn-default">Filtre</a>
                <a href="{{ route('property/edit/images',['id'=>$property->id]) }}" class="btn btn-xs btn-fill btn-info">Imagini</a>
                <a href="{{ route('property/edit/contacts',['id'=>$property->id]) }}" class="btn btn-xs btn-outline btn-default">Contacte asociate</a>
                <a href="{{ route('property/edit/seo',['id'=>$property->id]) }}" class="btn btn-xs btn-outline btn-default">Fisa proprietate</a>
              </p>
          </div>

          <!-- <div class="content "> -->
    <div class="content ">
      <div class="row">
        <div class="col-lg-12" style="text-align:left;">

          <div class="row">

            @if($property->images())
              @foreach($property->images as $propertyImage)
                <img src="{{asset('images/'.$propertyImage->image->file_name)}}" alt="" width="150px" height="150px" class="img-rounded img-thumbnail mr-3 ">
              @endforeach
            @endif





          </div>

          <form method="post" action="{{route('property/edit/uploadImages',['id' => $property->id])}}"  enctype="multipart/form-data" class="dropzone" id="dropzone">
            {{ csrf_field() }}
          </form>
              <div class="clearfix"></div>


      </div>
    </div>

      </div>
        </div>

        </div>
      </div>
    </div>
  </div>

@endsection

@section('modals')

@endsection
@include('admin.partials.modal_property_image_edit')

@section('jscripts')
<script>
$(document).ready(function(){


  //make the modal not close on mouse out or escape key
  $('#modalPropertyImageEdit').modal({
          backdrop: 'static',
          keyboard: false,
          show: false
  });

  //load the modal with dynamic content
  $('#modalPropertyImageEdit').on('show.bs.modal', function (event) {
    var image = $(event.relatedTarget); // Element that triggered the modal
    var propertyId = image.data('property-id');
    var propertyImageId = image.data('property-image-id');
    var modal = $(this);
    var url = '/property/'+propertyId+'/image/'+propertyImageId+'/edit';
    modal.find('.modal-body').load(url, function(){});
  })

  $(".triggerFiltersModal").click(function(){
    $('#modalPropertyImageEdit').modal('show');
  });


});


</script>
@endsection
