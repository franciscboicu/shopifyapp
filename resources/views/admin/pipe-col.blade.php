<div class="col-lg-3 col-md-12" style="margin-right:-20px;">
    <div class="card card-user">
        <div class="image" style="height:7px; background-color: {{$tab_color}};">
            <!-- <img src="admin/img/background.jpg" alt="..."> -->
        </div>
        <div class="content" style="min-height:20px;">
            <!-- <div class="author">
              <img class="avatar border-white" src="admin/img/faces/face-0.jpg" alt="...">
              <h4 class="title" style="margin-top:-10px;">Chet Faker<br>
                 <a href="#"><small>@chetfaker</small></a>
              </h4>
            </div> -->
            <h4 class="title text-center" style="margin-top:-10px;margin-bottom:-5px;">{{$tab_name}}   <small> <i class="pull-right ti-angle-down stageShowDetails" data-stage="{{$stage}}"></i></small> </h4>
        </div>
        <hr>
        <div class="text-center stageDetails{{$stage}}" style="background-color: #e7;" id="">
            <div class="row">
                <div class="col-md-3 col-md-offset-1">
                    <h5>{{ count($stageContacts[$stage]) }}<br><small>Deals</small></h5>
                </div>
                <div class="col-md-4">
                    <!-- <h5>2GB<br><small>Used</small></h5> -->
                </div>
                <div class="col-md-3">
                    <h5>24,6$<br><small>Value</small></h5>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <!-- <div class="header">
            <h4 class="title">Leads</h4>
        </div> -->
        <div class="content">
            <ul class="list-unstyled team-members" id="stageList{{$stage}}">
              @foreach($stageContacts[$stage] as $stageContact)
                <li class="">
                    <div class="row">
                        <div class="col-xs-3">
                            <div class="avatar">
                                <img src="admin/img/faces/face-{{$stage}}.jpg" alt="Circle Image" class="img-circle img-no-padding img-responsive">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            {{$stageContact}}
                            <br>
                            <span class="text-muted"><small>Offline</small></span>
                        </div>

                        <div class="col-xs-3 text-right">
                          @if($stage<=2)
                            <span data-stage="{{$stage}}" class="btn btn-sm btn-success btn-icon stageContact ti-arrow-right"></span>
                          @else
                            <span data-stage="{{$stage}}" class="btn btn-sm btn-warning btn-icon stageContact ti-arrow-down"></span>
                          @endif
                        </div>
                    </div>
                </li>
              @endforeach
            <!-- <li>
                <div class="row">
                    <div class="col-xs-3">
                        <div class="avatar">
                            <img src="admin/img/faces/face-1.jpg" alt="Circle Image" class="img-circle img-no-padding img-responsive">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        Creative Tim
                        <br>
                        <span class="text-success"><small>Available</small></span>
                    </div>

                    <div class="col-xs-3 text-right">
                      <button class="btn btn-sm btn-success btn-icon"><i class="fa fa-arrow-right"></i></button>
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="col-xs-3">
                        <div class="avatar">
                            <img src="admin/img/faces/face-0.jpg" alt="Circle Image" class="img-circle img-no-padding img-responsive">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        Flume
                        <br>
                        <span class="text-danger"><small>Busy</small></span>
                    </div>

                    <div class="col-xs-3 text-right">
                      <button class="btn btn-sm btn-success btn-icon"><i class="fa fa-arrow-right"></i></button>
                    </div>
                </div>
            </li> -->
        </ul>
        </div>
    </div>
</div>
