@extends('layouts.admin')
<?php use App\Property; ?>
@section('content')
<style>
  .save-warning{
    color: green;
    font-weight:bold;
  }
</style>
<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-12">

<div class="card">

  <div class="header">
      <h4 class="title"> (#{{$property->id}}) {{$property->title}} </h4>

      <p class="pull-right" style="margin-top:-30px;">
        <a href="{{ route('property/edit/filters',['id'=>$property->id]) }}" class="btn btn-xs btn-fill btn-info">Filtre</a>
        <a href="{{ route('property/edit/images',['id'=>$property->id]) }}" class="btn btn-xs btn-outline btn-default">Imagini</a>
        <a href="{{ route('property/edit/contacts',['id'=>$property->id]) }}" class="btn btn-xs btn-outline btn-default">Contacte asociate</a>
        <a href="{{ route('property/edit/seo',['id'=>$property->id]) }}" class="btn btn-xs btn-outline btn-default">Fisa proprietate</a>
      </p>
  </div>

  <!-- REMOVE CARD LATER-->
  <div class="card" style="border-top: 1px solid #ccc;">


    <div class="content">
    <button type="button" class="btn btn-primary btn-sm" id="triggerFiltersModal"
        data-toggle="modal" data-target="#filtersModal"
       ><span class="ti-plus  pull-right">

  Adauga filtre
</button>

    </div>
  </div>

  <!-- <div class="content "> -->
    <div class="content ">
      <div class="row">
        <div class="col-md-12" style="text-align:center;"  id="active-filters">

          @if(count($propertyFilters))
            @foreach($propertyFilters as $propertyFilter)
          <div class="row  property-filter-box" style="margin-bottom:5px;">

            <div class="col-md-4">
              <label class="pull-left"> {{ $propertyFilter->filter->filter_info }} <small style="" class="hidden save-warning">Apasa enter pentru a salva</small></label>

            </div>
            <style>
              .green{
                color:green;
              }
            </style>
            <div class="col-md-7 " >


                <div class="form-group">

                    @if($propertyFilter->filter->isSelectBox())

                    <select class="form-control property-filter col-md-10" name="transaction" data-filter-id="{{$propertyFilter->filter_id}}">
                      @foreach($propertyFilter->filter->isSelectBox() as $option)
                        <option value="{{$option}}" @if($propertyFilter->filter_value == $option) selected @endif>{{$option}}</option>
                      @endforeach
                    </select>

                    @else

                    <input type="text" data-filter-id="{{$propertyFilter->filter_id}}" name="{{$propertyFilter->filter->filter_name}}" class="property-filter form-control border-input" placeholder="Titlu" value="{{$propertyFilter->filter_value}}">

                    @endif

                </div>



            </div>
            <div class="col-md-1">

              <label class="pull-right property-filter-delete" data-filter-id="{{$propertyFilter->filter_id}}">[x]</label>


            </div>


          </div>
          @endforeach
        @else

        <br/><br/><br/>
                <img src="{{asset('admin/img/flag.png')}}"> <br/><br/>
          <button type="button" class="btn btn-primary btn-sm" id="triggerFiltersModal"
                data-toggle="modal" data-target="#filtersModal"
              ><span class="ti-plus  pull-right">

        Adauga filtre pentru aceasta proprietate</button>

        <br/><br/><br/>
        <br/><br/><br/>


        @endif

          <!-- <div class="text-center">
              <button type="submit"  class="btn btn-info btn-fill btn-wd">Salveaza</button>
          </div> -->
          <div class="clearfix"></div>


</div>
</div>

  </div>
</div>

</div>
</div>
</div>
</div>

@endsection

<!-- Include the Modal responsable with showing the filters-->
@include('admin.partials.modal_property_filters')

@section('jscripts')
<script>
$(document).ready(function(){

//make the modal not close on mouse out or escape key
$('#filtersModal').modal({
        backdrop: 'static',
        keyboard: false,
        show: false
});

//load the modal with dynamic content
$('#filtersModal').on('show.bs.modal', function (event) {

  var button = $(event.relatedTarget); // Button that triggered the modal

  alert(button);
  var modal = $(this);
  modal.find('.modal-body').load('{{route('property/filters/list', ['id'=>$property->id])}}', function(){});
});


//general function - TO BE MOVED
function notifySuccess(message){
  notify = $.notify({
      icon: 'ti-success',
      message: message
    },{
        type: 'success',
        timer: 1000
    });
}
//general function - TO BE MOVED
function notifyError(message){
  notify = $.notify({
      icon: 'ti-danger',
      message: message
    },{
        type: 'danger',
        timer: 1000
    });
};

/**
 * Sends a request to delete a property filter
 * @var e Event
 */
$('.property-filter-delete').click(function(e){

    e.preventDefault();
    $(this).closest('.property-filter-box').remove();

    var filter_id = $(this).data('filter-id');
    var property_id = {{ $property->id }};

    $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: "{{route('property/filter/remove')}}",
        data: {'property_id': property_id, 'filter_id': filter_id},
        success: function(response) {
          if(response.success == true){
            $('.available-filter[data-filter-id='+filter_id+']').removeClass('btn-fill');
            notifySuccess(response.message);
          }else{
            notifyError(response.message);
          }
        }
    });

});

  //to refactor to suite the new design
    $('.property-filter').click(function(){
      $(this).closest('.property-filter-box').find('.save-warning').show();
      console.log('here');
    });

/**
 * Sends a request to update the value of a filter
 * @var Event
 */
$('.property-filter').change(function(e){
  e.preventDefault();

  var property_id = {{ $property->id }};
  var filter_id   = $(this).data('filter-id');
  var filter_value = $(this).val();

  $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: "POST",
      url: "{{route('property/filter/update')}}",
      data: {'property_id': property_id, 'filter_id': filter_id, 'filter_value': filter_value},
      success: function(response) {
        if(response.success == true){
          notifySuccess(response.message);

        }else{
          notifyError(response.message);
        }
      }
  });

  $(this).closest('.property-filter-box').find('.save-warning').show();
});

});


</script>
@endsection
