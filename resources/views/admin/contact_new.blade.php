@extends('layouts.admin')
<?php use App\Contact; ?>

@section('content')

<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-12">

<div class="card">

  <div class="header">
      <h4 class="title">Contact nou</h4>
  </div>

  <div class="content ">


    <div class="row">
      <div class="col-lg-12" style="text-align:center;">
        <form action="{{route('contact/save')}}" method="POST" id="save">
          {{ csrf_field() }}
          @if($contact)
          <input type="hidden"  name="name" class="form-control border-input" placeholder="Titlu" value="{{$contact->id}}">
          @endif
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label class="pull-left">Tip Contact</label>
                    <select class="form-control" name="contact_type">
                      @foreach(Contact::TYPES as $contactTypeId => $contactTypeLabel)
                      <option value="{{$contactTypeId}}">{{$contactTypeLabel}}</option>
                      @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="pull-left">Nume</label>
                    <input type="text"  name="name" class="form-control border-input" placeholder="Titlu" value="@if($contact){{$contact->name}}@endif">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="pull-left">Telefon</label>
                    <input type="text"  name="phone_numbers" class="form-control border-input" placeholder="Titlu" value="@if($contact){{$contact->phone_numbers}}@endif">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="pull-left">Email</label>
                    <input type="text"  name="email" class="form-control border-input" placeholder="Titlu" value="@if($contact){{$contact->email}}@endif">
                </div>
            </div>
        </div>
        <div class="text-center">
            <button type="submit"  class="btn btn-info btn-fill btn-wd">Continua</button>
        </div>
        <div class="clearfix"></div>
    </form>


    </div>
    </div>



  </div>
</div>

</div>
</div>
</div>
</div>

@endsection

@section('jscripts')
<script>


$('#save').on('submit', function(e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: "{{route('contact/save')}}",
        data: $(this).serialize(),
        success: function(response) {
        console.log(response);
          if(response.success == true){
            if(response.is_redirect == true){
              window.location.href = response.redirect_url;
            }
          }else{
            alert(false);
          }
        }
    });
});

</script>
@endsection
