<div class="row">
<div class="col-lg-6">

    @foreach($availableFilters as $availableFilter)
        <button data-keyboard="false" data-backdrop="static" data-filter-id="{{$availableFilter->id}}" class="btn btn-success btn-md btn-block available-filter2"><span class="ti-angle-right  pull-right"></span>{{$availableFilter->filter_info}}</button>
    @endforeach

</div>
<div class="col-lg-6" id="fakeFiltersList">


</div>

</div>

<script>

$(document).ajaxComplete(function(){

    var addedFilters = [];
    var id = null;
    var text = null;
    $('.available-filter2').click(function(){
        var id = $(this).data("filter-id");
        var text = $(this).text();
        if($.inArray(id, addedFilters)!='-1'){
            console.log("Already added!");
            return;
        }

        addVisualFilter(id, text);
        $(this).addClass('btn-fill');
        addedFilters.push(id);
        checkIfSaveCanBeSeen();

        console.log(addedFilters);
    });

    $(document).on("click",".added-filter",function () {
      var id = $(this).data("filter-id");
      removeItem(id);
      $(this).remove();
    });

    function removeItem(item){
      for(i in addedFilters){
          if(addedFilters[i]===item){
              addedFilters.splice(i,item);
              break;
          }
      }
    }

    function checkIfSaveCanBeSeen(){
        if(addedFilters.length !== 0){
            $("#saveFilters").removeAttr('disabled');
            return true;
        }
        $("#saveFilters").attr('disabled', 'disabled');
    }

    /**
    * Sync added filters to backend, when Closing the modal
     */
    $('#filtersModal').on('hide.bs.modal', function (event) {


    })

    //Push a fake element to the right column
    function addVisualFilter(id, name){
        fakeElement =  '<button data-filter-id="'+id+'" class="btn btn-success btn-md btn-block available-filter2 added-filter"><span class="pull-left"></span>'+name+'</button>';
        $("#fakeFiltersList").append(fakeElement);
    }



    //makes an ajax request for all id's in the array to save the filter
    $('#saveFilters').click(function(){
        if(addedFilters.length !== 0){

            $.each(addedFilters, function( index, value ) {
                $.ajax({
                    url: "/filter/add/"+value+"/{{$property->id}}",
                    error: function(){
                        // will fire when timeout is reached
                        //alert("NotOkButContinue");
                    },
                    success: function(){
                        //alert("Ok");
                    },
                    timeout: 1000 // sets timeout to 3 seconds
                });
            });

            $('#filtersModal').modal('hide');


        }
    });

})



</script>
