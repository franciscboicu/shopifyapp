<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul>

                <li>
                    <a href="#">
                        Weasel Apps
                    </a>
                </li>
                <li>
                    <a href="#">
                       Support
                    </a>
                </li>
                <li>
                    <a href="#">
                        Data management
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright pull-right">
            &copy; <script>document.write(new Date().getFullYear())</script>, crafted with <i class="fa fa-heart heart"></i> by <a href="#">Weasel</a>
        </div>
    </div>
</footer>
