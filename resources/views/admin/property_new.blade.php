@extends('layouts.admin')
<?php use App\Property; ?>
@section('content')

<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-12">

<div class="card">

  <div class="header">
      <h4 class="title">Proprietate noua</h4>
  </div>

  <!-- <div class="content "> -->
    <div class="content ">
      <div class="row">
        <div class="col-lg-12" style="text-align:center;">
          <form action="{{route('property/save')}}" method="POST" id="save">
            {{ csrf_field() }}

          <div class="row">
            <div class="col-md-4"></div>
              <div class="col-md-4">
                  <div class="form-group">
                      <label class="pull-left">Categorie</label>
                      <select class="form-control" name="category">
                        @foreach(Property::CATEGORY as $categoryId => $categoryName)
                        <option value="{{$categoryId}}">{{$categoryName}}</option>
                        @endforeach
                      </select>
                  </div>
              </div>
              <div class="col-md-4"></div>
          </div>
          <div class="row">
            <div class="col-md-4"></div>
              <div class="col-md-4">
                  <div class="form-group">
                      <label class="pull-left">Tip Tranzactie</label>
                      <select class="form-control" name="transaction">
                        @foreach(Property::TRANS as $transId => $transName)
                        <option value="{{$transId}}">{{$transName}}</option>
                        @endforeach
                      </select>
                  </div>
              </div>
              <div class="col-md-4"></div>
          </div>


          <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
              <label> <small style="color: green;">Titlul, descrierea si textele specifice SEO, vor fi auto-generate pe baza filtrelor.</small></label>
              <label> Mergi la pasul urmator pentru a introduce filtre specifice categoriei selectate.</label>
            </div>
            <div class="col-md-4"></div>
          </div>

          <br/><br/>

          <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <button type="submit"  class="btn btn-info btn-fill btn-wd btn-block">Pasul urmator</button>
            </div>
            <div class="col-md-4"></div>

          </div>


          <div class="clearfix"></div>
      </form>


</div>
</div>

  </div>
</div>

</div>
</div>
</div>
</div>

@endsection

@section('jscripts')
<script>


$('#save').on('submit', function(e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: "{{route('property/save')}}",
        data: $(this).serialize(),
        success: function(response) {
        console.log(response);
          if(response.success == true){
            if(response.is_redirect == true){
              window.location.href = response.redirect_url;
            }
          }else{
            alert(false);
          }
        }
    });
});

</script>
@endsection
