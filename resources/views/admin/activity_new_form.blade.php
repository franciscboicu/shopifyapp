<?php use App\Contact; ?>

<div class="card hidden" id="formNewActivity">

  <div class="header">
      <h4 class="title">Activitate</h4>
      <p class="pull-right" style="margin-top:-37px;">
        <span class="ti-close" id="closeAddNewActivityForm"></span>
      </p>
  </div>

  <div class="content ">


    <div class="row">
      <div class="col-lg-12" style="text-align:center;">
        <form action="{{route('activity/save')}}" method="POST" id="save">
          {{ csrf_field() }}

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label class="pull-left">Title</label>
                    <input type="text"  name="title" class="form-control border-input" placeholder="Titlu" value="">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="pull-left">Description</label>
                    <input type="text"  name="description" class="form-control border-input" placeholder="Descriere" value="">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="pull-left">Data / Ora Activitatii</label>
                    <input type="text"  name="starts_at" class="form-control border-input" placeholder="Titlu" value="">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="pull-left">Estimare durata</label>
                    <input type="text"  name="ends_at" class="form-control border-input" placeholder="Titlu" value="">
                </div>
            </div>
        </div>
        <div class="text-center">
            <button type="submit"  class="btn btn-info btn-fill btn-wd">Continua</button>
        </div>
        <div class="clearfix"></div>
    </form>


    </div>
    </div>



  </div>
</div>
