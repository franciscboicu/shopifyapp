@extends('layouts.admin')


@section('content')
<style>

.statusButton{
    margin-top:-50px;
}
</style>
<div class="content">
    <div class="container-fluid">

            <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-warning text-center">
                                            <i class="ti-server"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Synced Products</p>
                                            {{count([])}}
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <a href=""><i class="ti-reload"></i> Updated now</a>
                                        <small class="">Last updated: <strong>3 days ago</strong></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-success text-center">
                                            <i class="ti-wallet"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Returns to page</p>
                                           {{$returnsFromDynamicTitle}}
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-calendar"></i> People that returned to store due to dynamic title
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-danger text-center">
                                            <i class="ti-pulse"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Support Tickets </p>
                                            <strong  style="color:red">1</strong>/<strong style="color:green">3</strong>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <button type="" style="position:relative;margin-left:80px;"class="btn btn-info btn-xs btn-fill btn-wd"><i class="ti-help-alt"></i>Open Ticket</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-info text-center">
                                            <i class="ti-twitter-alt"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Reviews</p>
                                            +145
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-reload"></i> 4.7 stars rating
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


        @if($userStores && ($userStores->first()->script_tags_installed !== NULL))
            @foreach($userStores as $userStore)
            <div class="row">
                <div class="col-md-12">
                    <div class="card" >
                        <div class="header">
                            <h4 class="title">{{$userStore->name}}</h4>
                            <p class="category">
                                <a href="https://{{$userStore->domain}}" target="_blank">{{$userStore->domain}}</a>
                            </p>

                        </div>
                        <div class="content">


                            @foreach($userStore->modules as $storeModule)

                            <div class="card" style="border-top:1px solid #ccc;">
                                <div class="header">
                                    <h4 class="title">

                                        <a href="{{route('admin/module/view', ['slug' => $storeModule->module->slug])}}">
                                            <strong>{{$storeModule->module->name}}</strong>
                                        </a>
                                    </h4>
                                    <p class="category p-3">
                                        {{$storeModule->module->short_description}}
                                    </p>

                                    {{-- <a href="" class="btn btn-success pull-right statusButton" style="margin-right:100px;">VIEW DATA</a> --}}

                                      @if($storeModule->enabled == 1)
                                      <a href="{{route('admin/module/changestatus', ['slug' => $storeModule->module->slug, 'redirectTo' => 'dashboard' ])}}" class="btn btn-success pull-right statusButton" style="">ONLINE</a>
                                      @else
                                      <a href="{{route('admin/module/changestatus', ['slug' => $storeModule->module->slug, 'redirectTo' => 'dashboard' ])}}" class="btn btn-default pull-right statusButton" style="">OFFLINE</a>
                                      @endif

                                </div>

                            </div>

                            @endforeach


                            <div class="footer">

                                <hr>
                                <div class="stats">
                                    <i class="ti-timer"></i> Campaign sent 2 days ago
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach


        @else

        <div class="card" style="border-top:1px solid #ccc;">
                <div class="header">
                    <h4 class="title">

                        <a href="">
                            <strong>Install our app to your shop by pushing our custom js to your script tags</strong>
                        </a>
                    </h4>
                    <p class="category p-3">
                        This will publish our custom js file to your theme source, to be loaded on your store.
                    </p>

                    <a href="{{route('admin/install/scripts')}}" class="btn btn-success pull-right statusButton" style="">INSTALL SCRIPT TAGS</a>

                </div>

            </div>

        @endif



    </div>
</div>

@endsection


@section('jscripts')


@endsection
