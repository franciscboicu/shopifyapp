//push the jquery to the page
var jq = document.createElement("script");
jq.addEventListener("load", proceed);
jq.src = "//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js";
document.querySelector("head").appendChild(jq);

//push the css into the page
var css = document.createElement("link");
css.href = "<?=env('APP_URL');?>css/remote.css?v=<?=time();?>";
css.rel = "stylesheet";
document.querySelector("head").appendChild(css);

//load the settings
//load the selectors
//load the server data
//load the STORE data
function proceed () {

$(document).ready(function(){
	var SETTINGS = jQuery.parseJSON( '<?=json_encode($moduleSettings);?>' );
	var SELECTORS = jQuery.parseJSON( '<?=json_encode($storeSelectors);?>' );
	var SERVER = jQuery.parseJSON( '<?=json_encode($server);?>' );
	var STORE = jQuery.parseJSON( '<?=json_encode($store);?>' );

	//iterate through each module and push the respective blade html
	//that contains the js, in a self run manner
	@if(count($moduleJsFiles) > 0)
		@foreach($moduleJsFiles as $moduleJsFile)
			<?=$moduleJsFile; ?>
		@endforeach
	@else
		console.log("No modules loaded");
	@endif
});

}
